## bookmarks

[[_TOC_]]

### debian knoife
---


#### setting keyboard shortcuts

https://www.reddit.com/r/crunchbangplusplus/comments/arkhh5/how_to_set_keyboard_shortcuts/?rdt=44640

How to set keyboard shortcuts
I'm trying to set all the keyboard shortcuts to work correctly.

For instance, I'd want fn+up/down to control the volume.

I thought that none of them worked, but I just realized that indeed some are (fn+left/right control the light). This suggests that there is, somewhere in the system, some document that controls all these shortcuts and their related commands. Where do I find it?

EDIT:

Reading other posts I reached ~/.config/openbox/rc.xml. Do you confirm me that's the file I'm looking for?

#### apple_hid keyboard options
https://wiki.archlinux.org/title/Apple_Keyboard

#### Remapping Caps Lock to Control and Escape (not the usual way)

https://askubuntu.com/questions/177824/remapping-caps-lock-to-control-and-escape-not-the-usual-way

`setxkbmap -option 'caps:ctrl_modifier' && xcape -e 'Caps_Lock=Escape' &`

#### How to swap Command and Control keys with xkb step by step? [not what I ended up using] 
https://askubuntu.com/questions/501659/how-to-swap-command-and-control-keys-with-xkb-step-by-step

#### [SOLVED] OpenBox - keybindings for volume/brightness not working
https://bbs.archlinux.org/viewtopic.php?id=190943

I've edited my ./config/openbox/rc.xml file to reflect the XF86 keybindings under the Keybindings section, for volumeup, volumedown, and volumemute:

```<keybind key="XF86AudioRaiseVolume">
      <action name="Execute">
        <command>amixer set Master 10%+</command>
      </action>
   </keybind>
    <keybind key="XF86AudioLowerVolume">
     <action name="Execute">
        <command>amixer set Master 10%-</command>
      </action>
    </keybind>
    <keybind key="XF86AudioMute">
      <action name="Execute">
        <command>amixer set Master toggle</command>volumeicon installed, and I can change the volume by scrolling
      </action>
   </keybind>
```

---
```
I think you have to use the keycodes:
Raise volume is "0x1008ff13"
Lower volume is "0x1008ff11"
Mute is "0x1008ff12"
```

#### openbox touchpad actions (SOLVED)
https://forums.debian.net/viewtopic.php?t=109527

#### xSwipe
xSwipe is multitouch gesture recognizer. This script make your linux PC able to recognize swipes.

https://github.com/iberianpig/xSwipe


#### pretty ff theme

https://github.com/vinceliuice/WhiteSur-firefox-theme
